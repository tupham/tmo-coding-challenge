1. What is done well?
- Code structure in general with html, ts, css, spec are in the same directory

2. What would you change?
- The libs for frontend part should stay in `stocks` directory. It was a bit confused for me to look through the required files
- Also some ui components in libs don't look like they are shared in other places. I recomment put them under app in `stocks`
- Add more styles for the page

3. Are there any code smells or problematic implementations
- Readme proxy link is incorrect
- Chart is not displaying
- Chart overlaps with Ok butotn
